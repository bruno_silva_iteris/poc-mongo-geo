# Mongo com geolocalização

> Uma POC contendo a pesquisa de latitude e longitude por meio de um endereço e o cálculo de distância utilizando MongoDB

## Mongo no Docker

``` bash
# Caso não queira instalar o mongo, segue comandos para rodar no Docker (mongo-student pode ser alterado para qualquer nome)

# Criar volume 
docker volume create mongo-student

# Rodar imagem
docker run -p 27017:27017 --name mongo-student -v mongo-student:/data/db -d mongo

# Caso queira conectar no mongo
docker exec -it mongo-student mongo
```

## Conectar no Mongo e executar os comandos abaixo

``` bash
# Criar a colecao
db.createCollection("students")

# Criar o índice
db.students.createIndex({
    contact: "2dsphere"
})

# Exemplo de consulta
# Retorna 3 (num) pontos proximos do que foi passado (near.coordinates)
# A distancia calculada é retornada no campo "distanceField"
# O skip é para pular a coordenada passado no near (pois a distancia vai ser zero, entao sera o primeiro a ser retornado)
# pretty para melhorar o retorno (visualmente)
db.students.aggregate([
{
    $geoNear : {
        near : {
            coordinates: [-26.2616239, -48.8175132],
            type : "Point"
        },
        distanceField : "distancia.calculada",
        spherical : true,
        num : 3
    }
},
{ 
    $skip :1 
}
])
.pretty()

```

## Front (Vue.js)

``` bash
# Instalar dependencias
npm install

# Rodar localmente
npm run dev
```

## Swagger
[Swagger](http://localhost:8080/swagger-ui.html)
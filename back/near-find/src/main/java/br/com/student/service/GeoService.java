package br.com.student.service;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.Geometry;
import com.google.maps.model.LatLng;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
public class GeoService {

    private static final String API_MAP_KEY = "AIzaSyCdsEr79iuSY08S0SMFewin0JxcOchaAds";

    public List<Double> getLatLng(String address) throws InterruptedException, ApiException, IOException {

        GeoApiContext context = new GeoApiContext.Builder().apiKey(API_MAP_KEY).build();
        GeocodingApiRequest request = GeocodingApi.newRequest(context).address(address);

        GeocodingResult[] results = request.await();
        GeocodingResult resultado = results[0];

        Geometry geometry = resultado.geometry;
        LatLng location = geometry.location;

        return Arrays.asList(location.lat, location.lng);
    }

}

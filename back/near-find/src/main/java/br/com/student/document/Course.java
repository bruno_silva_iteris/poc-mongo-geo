package br.com.student.document;

public class Course {
    private String nome;

    public Course() {}

    public Course(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}

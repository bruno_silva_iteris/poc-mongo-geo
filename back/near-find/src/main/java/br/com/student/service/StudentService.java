package br.com.student.service;

import br.com.student.document.Student;
import br.com.student.dto.ClusterDTO;
import br.com.student.repository.StudentRepository;
import br.com.student.utils.ClusterUtils;
import ch.hsr.geohash.GeoHash;
import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;
import com.mongodb.client.model.geojson.Position;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.apache.commons.math3.ml.clustering.DoublePoint;
import org.apache.commons.math3.util.MathUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StudentService {

    @Autowired
    private StudentRepository repository;

    @Autowired
    private GeoService geoService;

    public List<Student> findAll() {
        return this.repository.findAll();
    }

    public Optional<Student> findById(String id) {
        return this.repository.findById(id);
    }

    public List<Student> findByHash(String hash) {
        return this.repository.findByHash(hash);
    }

    public List<Student> findNear(Student student, long limit, long skip) {

        List<Double> coordinates = student.getContact().getCoordinates();
        Point point = new Point(coordinates.get(0), coordinates.get(1));
        Distance distance = new Distance(5, Metrics.KILOMETERS);

        List<Student> list = this.repository.findByContactNear(point, distance);

        return list.stream()
                    .skip(skip)
                    .limit(limit)
                    .collect(Collectors.toList());
    }

    public Student save(Student student) {

        if (student.getContact() != null) {
            try {

                List<Double> coordinates = geoService.getLatLng(student.getContact().getAddress());

                student.getContact().setCoordinates(coordinates);
                student.setGeoHash( getGeoHash(coordinates) );

            } catch (InterruptedException | ApiException | IOException e) {
                e.printStackTrace();
            }
        }

        return this.repository.save(student);
    }

    public Optional<LatLng> findLatLngByAddress(String address) {
        try {

            List<Double> coordinates = geoService.getLatLng(address);
            LatLng latLng = new LatLng(coordinates.get(0), coordinates.get(1));

            return Optional.of(latLng);

        } catch (InterruptedException | ApiException | IOException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public List<ClusterDTO> getClusters(List<Student> students) {

        List<ClusterDTO> resultClusters = new ArrayList<>();

        double eps = .05; // maximum radius of the neighborhood to be considered
        int minPoints = 3; // minimum number of points needed for a cluster

        DBSCANClusterer dbscan = new DBSCANClusterer(eps, minPoints);
        List<Cluster<DoublePoint>> clusters = dbscan.cluster(getPoints(students));

        for (Cluster<DoublePoint> cluster: clusters)
            resultClusters.add(
                    new ClusterDTO().withCount(cluster.getPoints().size())
                                    .withCenter(ClusterUtils.getCenterByPoints(cluster.getPoints()))
                                    .withPoints(cluster.getPoints())
            );

        return resultClusters;
    }

    private List<DoublePoint> getPoints(List<Student> students) {

        List<DoublePoint> points = new ArrayList<>();

        for (Student s: students) {
            double[] d = new double[2];
            d[0] = s.getContact().getCoordinates().get(0);
            d[1] = s.getContact().getCoordinates().get(1);

            points.add(new DoublePoint(d));
        }

        return points;
    }

    private String getGeoHash(List<Double> coordinates) {
        GeoHash hash = GeoHash.withCharacterPrecision(coordinates.get(0), coordinates.get(1), 12);
        return hash.toBase32();
    }

    public static void main(String[] args) {

        GeoHash hash = GeoHash.withCharacterPrecision(-23.633664, -46.714705, 12);

        System.out.println(hash.getPoint().getLatitude());
        System.out.println(hash.getPoint().getLongitude());
        System.out.println("Hash: " + hash.toBase32());

    }

}

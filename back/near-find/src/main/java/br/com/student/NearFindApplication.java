package br.com.student;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NearFindApplication {

	public static void main(String[] args) {
		SpringApplication.run(NearFindApplication.class, args);
	}
}

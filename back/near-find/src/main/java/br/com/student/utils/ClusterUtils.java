package br.com.student.utils;

import org.apache.commons.math3.ml.clustering.DoublePoint;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ClusterUtils {

    private ClusterUtils() {}

    public static DoublePoint getCenterByPoints(List<DoublePoint> points) {

        List<Double> latitudes = getLatitudes(points);
        List<Double> longitudes = getLongitudes(points);

        return getCenterPoint(latitudes, longitudes);
    }

    private static DoublePoint getCenterPoint(List<Double> latitudes, List<Double> longitudes) {
        Double minLat = Collections.min(latitudes);
        Double maxLat = Collections.max(latitudes);

        Double minLng = Collections.min(longitudes);
        Double maxLng = Collections.max(longitudes);

        double center[] = {(minLat + maxLat) / 2d,
                (minLng + maxLng) / 2d};

        return new DoublePoint(center);
    }

    private static List<Double> getLatitudes(List<DoublePoint> list) {
        return mapDouble(list, 0);
    }

    private static List<Double> getLongitudes(List<DoublePoint> list) {
        return mapDouble(list, 1);
    }

    private static List<Double> mapDouble(List<DoublePoint> list, int index) {
        return list.stream()
                   .map(p -> p.getPoint()[index])
                   .collect(Collectors.toList());
    }

}

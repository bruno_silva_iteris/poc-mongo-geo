package br.com.student.repository;

import br.com.student.document.Student;
import org.bson.types.ObjectId;
import org.springframework.data.geo.Point;
import org.springframework.data.geo.Distance;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface StudentRepository extends MongoRepository<Student, String> {

    // https://docs.spring.io/spring-data/mongodb/docs/1.4.0.RELEASE/reference/htmlsingle/#mongodb.repositories.queries.geo-spatial
    List<Student> findByContactNear(Point p, Distance d);

    @Query("{'geo_hash': {$regex : ?0, $options: 'i'}}")
    List<Student> findByHash(String hash);
}

package br.com.student.dto;

import br.com.student.document.Contact;
import br.com.student.document.Student;

public class StudentDTO {

    private String name;
    private ContactDTO contact;

    public Student toModel() {
        Student student = new Student();
        student.setName(this.name);

        Contact contact = new Contact(this.contact.getAddress());
        student.setContact(contact);

        return student;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ContactDTO getContact() {
        return contact;
    }

    public void setContact(ContactDTO contact) {
        this.contact = contact;
    }
}

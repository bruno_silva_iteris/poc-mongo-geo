package br.com.student.controller;

import br.com.student.document.Student;
import br.com.student.dto.ClusterDTO;
import br.com.student.dto.StudentDTO;
import br.com.student.service.StudentService;
import com.google.maps.model.LatLng;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
public class StudentController {

    @Autowired
    private StudentService service;

    @ApiOperation("Lista todos os estudantes")
    @GetMapping("/students")
    public ResponseEntity<List<Student>> findAll() {
        List<Student> list = service.findAll();
        return ResponseEntity.ok(list);
    }

    @ApiOperation("Busca um estudante por ID")
    @GetMapping("/students/{studentId}")
    public ResponseEntity<Student> findById(@PathVariable String studentId) {
        Optional<Student> student = service.findById(studentId);
        if (!student.isPresent())
            return ResponseEntity.noContent().build();
        return ResponseEntity.ok(student.get());
    }

    @ApiOperation("Procura estudantes próximos a um outro estudante. " +
                  "Parâmetros via query: limite de estudantes (limit) e se deseja pular o aluno referência (skip).")
    @GetMapping("/students/{studentId}/near")
    public ResponseEntity<List<Student>> findNear(@PathVariable String studentId,
                                                  @RequestParam("limit") long limit,
                                                  @RequestParam("skip") long skip) {
        Optional<Student> student = service.findById(studentId);
        if (!student.isPresent())
            return ResponseEntity.noContent().build();
        List<Student> list = service.findNear(student.get(), limit, skip);
        return ResponseEntity.ok(list);
    }

    @ApiOperation("Retorna latitude e longitude baseado em um endereço")
    @GetMapping("/students/coordinates")
    public ResponseEntity<LatLng> findCoordinatesByAddress(@RequestParam("q") String address) {
        Optional<LatLng> latLng = service.findLatLngByAddress(address);
        if (!latLng.isPresent())
            return ResponseEntity.noContent().build();
        return ResponseEntity.ok(latLng.get());
    }

    @ApiOperation("Salva um estudante. A latitude e longitude será buscada utilizando o endereço passado no contato.")
    @PostMapping("/students")
    public ResponseEntity<Student> save(@RequestBody StudentDTO dto) {
        Student student = service.save(dto.toModel());

        URI location = ServletUriComponentsBuilder
                                .fromCurrentRequest().path("/{studentId}")
                                .buildAndExpand(student.getId()).toUri();

        return ResponseEntity.created(location).body(student);
    }

    @ApiOperation("Lista estudantes pelo hash")
    @GetMapping("/students/coordinates/hash")
    public ResponseEntity<List<Student>> findByHash(@RequestParam("q") String hash) {
        List<Student> list = service.findByHash(hash);
        return ResponseEntity.ok(list);
    }

    @ApiOperation("Cluster das coordenadas de todos os estudantes ou filtrando pelo hash")
    @GetMapping("/students/coordinates/cluster")
    public ResponseEntity<List<ClusterDTO>> findGeoHash(@RequestParam(value = "hash", required = false) String hash) {
        List<Student> students = (hash == null) ? service.findAll() : service.findByHash(hash);
        List<ClusterDTO> clusters = service.getClusters(students);
        return ResponseEntity.ok(clusters);
    }

}

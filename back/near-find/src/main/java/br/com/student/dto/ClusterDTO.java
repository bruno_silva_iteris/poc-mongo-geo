package br.com.student.dto;

import org.apache.commons.math3.ml.clustering.DoublePoint;

import java.util.List;

public class ClusterDTO {

    private int count;
    private DoublePoint center;
    private List<DoublePoint> points;

    public ClusterDTO withCount(int count) {
        this.count = count;
        return this;
    }

    public ClusterDTO withCenter(DoublePoint center) {
        this.center = center;
        return this;
    }

    public ClusterDTO withPoints(List<DoublePoint> points) {
        this.points = points;
        return this;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public DoublePoint getCenter() {
        return center;
    }

    public void setCenter(DoublePoint center) {
        this.center = center;
    }

    public List<DoublePoint> getPoints() {
        return points;
    }

    public void setPoints(List<DoublePoint> points) {
        this.points = points;
    }
}

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import axios from 'axios'
import router from './router'
import * as VueGoogleMaps from 'vue2-google-maps'

import 'bulma/css/bulma.css'

Vue.config.productionTip = false

axios.defaults.baseURL = 'http://localhost:8080/';

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCdsEr79iuSY08S0SMFewin0JxcOchaAds',
    libraries: 'places'
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

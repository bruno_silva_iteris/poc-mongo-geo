import axios from 'axios'

export default class StudentService {

    findStudents() {
        return axios.get(`students`)
    }

    findNear(studentId, limit, skip) {
        return axios.get(`students/${studentId}/near?limit=${limit}&skip=${skip}`)
    }

    findCoordinatesByAddress(address) {
        return axios.get(`students/coordinates?q=${address}`)
    }

    findClusters() {
        return axios.get(`students/coordinates/cluster`)
    }

    save(student) {
        return axios.post(`students`, student)
    }

}
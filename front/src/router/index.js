import Vue from 'vue'
import Router from 'vue-router'
import StudentList from '@/components/StudentList'
import StudentForm from '@/components/StudentForm'
import ClusterView from '@/components/ClusterView'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'StudentList',
      component: StudentList
    },
    {
      path: '/create',
      name: 'StudentForm',
      component: StudentForm
    },
    {
      path: '/cluster',
      name: 'ClusterView',
      component: ClusterView
    }
  ]
})
